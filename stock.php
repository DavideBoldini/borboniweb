<?php

require_once("./bootstrap.php");
define("Title", "<title>Giacenza</title>");

if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin") {
    
    header("location: index.php");
}

$css = array("css/info.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "stockPage.php";
$templateParams["head"] = "headWithJSPage.php";
$table = $dbh->getStock();

if(count($_GET)!=0 && $_GET["stockArticleCode"]!=""){
    $articleStock = $dbh->searchArticleInStock($_GET["stockArticleCode"]);
    $table = $articleStock;
}

require("template/base.php");
?>
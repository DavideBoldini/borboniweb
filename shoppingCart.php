<?php

require_once("./bootstrap.php");
define("Title", "<title>Carrello</title>");


if (!isset($_SESSION["email"])) {
    header("location: login.php");
} else {
    $profileVal = $dbh->getPersonalInfo($_SESSION["email"]);
}

$css = array("css/index.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "shoppingCartPage.php";
$templateParams["head"] = "headWithJSPage.php";
$templateParams["articoli_carrello"] = $dbh->getShoppingCart($_SESSION["email"]);

$_SESSION["articlesInCart"] = $templateParams["articoli_carrello"];



require("template/base.php");
?>
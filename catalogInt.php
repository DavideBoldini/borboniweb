<?php

require_once("./bootstrap.php");
define("Title", "<title>Catalogo Interni</title>");

$css = array("css/catalogue.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "catalogPage.php";
$templateParams["head"] = "headWithJSPage.php";
$templateParams["articoli"]=$dbh->getInteriorCatalogue();


require("template/base.php");
?>
<?php

require_once("./bootstrap.php");
define("Title", "<title>Home</title>");

$css = array("css/index.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "bodyPage.php";
$templateParams["head"] = "headPage.php";
$templateParams["articoli_rilevanti"] = $dbh->getFeaturedArticle();
$templateParams["articoli_natalizi"] = $dbh->getChristmasArticle();
$templateParams["articoli_nuovi"] = $dbh->getLatestArticle();
require("template/base.php");
?>
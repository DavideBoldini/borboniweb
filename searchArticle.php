<?php
    require_once("./bootstrap.php");
    
    define("Title", "<title>Risultati ricerca</title>");

    $css = array("css/catalogue.css", "css/header.css");
    $templateParams["header"] = "headerPage.php";
    $templateParams["footer"] = "footerPage.php";
    $templateParams["body"] = "catalogPage.php";
    $templateParams["head"] = "headPage.php";
    $templateParams["articoli"] = $dbh -> searchArticle($_GET["articleName"]);


    require("template/base.php");
    
?>
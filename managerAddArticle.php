<?php

require_once("./bootstrap.php");
define("Title", "<title>Aggiungi Articolo</title>");

$css = array("css/managerAddArticle.css", "css/header.css");
if (!isset($_SESSION["email"])) {
    header("location: login.php");
}
if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin") {
    
    header("location: index.php");
}
$templateParams["head"] = "headWithJSPage.php";
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "managerAddArticlePage.php";


require("template/base.php");
?>
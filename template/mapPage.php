<div class="row">
    <div class="col-12 px-0 pt-3">
        <aside>
            <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                <li>
                    <p class="font-weight-bold" id="faq">
                        &#128205; Mappa del sito
                    </p>
                </li>
            </ul>
        </aside>
    </div>
</div>

<div class="row m-3">
    <div class="col-12 col-md-4 card">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#128161; Home</h5>
            <h6 class="card-subtitle mb-2 text-muted">Pagina iniziale</h6>
            <p class="card-text">Pagina principale di Borboni Luce con nuove offerte e arrivi!</p>
            <a href="index.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
</div>

<div class="row m-3">
    <div class="col-12 col-md-3 card mr-2">
        <div class="card-body">
            <h5 class="card-title font-weight-bold"> &#127968; Prodotti Interni</h5>
            <h6 class="card-subtitle mb-2 text-muted">Catalogo</h6>
            <p class="card-text">Catalogo dedicato ai migliori articoli per gli interni. Non perderteli!</p>
            <a href="catalogInt.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
    <div class="col-12 col-md-3 card mr-2">
        <div class="card-body">
            <h5 class="card-title font-weight-bold"> &#127795; Prodotti Esterni</h5>
            <h6 class="card-subtitle mb-2 text-muted">Catalogo</h6>
            <p class="card-text">Catalogo dedicato ai migliori articoli per gli esterni. Non perderteli!</p>
            <a href="catalogExt.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>

    <div class="col-12 col-md-3 card">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#127774; Pannelli Solari</h5>
            <h6 class="card-subtitle mb-2 text-muted">Catalogo</h6>
            <p class="card-text">Un occhio anche verso l'energia rinnovabile con i migliori pannelli solari.</p>
            <a href="catalogPanel.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
</div>

<div class="row m-3">
    <div class="col-12 col-md-3 card mr-2">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#128100; Profilo</h5>
            <h6 class="card-subtitle mb-2 text-muted">Account</h6>
            <p class="card-text">Pagina dedicata al tuo profilo.</p>
            <a href="profile.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
    <div class="col-12 col-md-3 card mr-2">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#128273; Login</h5>
            <h6 class="card-subtitle mb-2 text-muted">Account</h6>
            <p class="card-text">Pagina per accedere facilmente ai servizi di Borboni Luce.</p>
            <a href="login.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
    <div class="col-12 col-md-3 card">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#128274; Registrati</h5>
            <h6 class="card-subtitle mb-2 text-muted">Account</h6>
            <p class="card-text">Registrati a Borboni Luce.</p>
            <a href="register.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
</div>

<div class="row m-3">
    <div class="col-12 col-md-3 card mr-2">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#127970; Chi siamo</h5>
            <h6 class="card-subtitle mb-2 text-muted">Assistenza</h6>
            <p class="card-text">Pagina per leggere tutta la storia e la crescita dell'azienda Borboni Luce.</p>
            <a href="info.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
    <div class="col-12 col-md-3 card">
        <div class="card-body">
            <h5 class="card-title font-weight-bold">&#10067; FAQ</h5>
            <h6 class="card-subtitle mb-2 text-muted">Assistenza</h6>
            <p class="card-text">Pagina con le domande e risposte più frequenti.</p>
            <a href="faq.php" class="btn btn-primary">Vai alla pagina</a>
        </div>
    </div>
</div>
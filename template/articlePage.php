            <div class="row mb-4">
                <div class="col-12 col-md-4 mt-3 py-3 px-3 ml-2">
                    <img src="<?php echo IMG_DIR.$templateParams["articleClicked"][0]["Immagine"]?>" class="img-fluid"
                        alt="immagine articolo" id="imgArticle" />
                </div>
                <div class="col-md-1"></div>
                <div class=" col-12 col-md-6 font-weight-bold mt-4">
                    <h1 class="font-weight-bold"><?php echo $templateParams["articleClicked"][0]["Nome"]?></h1>
                    <h2 style="word-wrap: break-word;"><?php echo $templateParams["articleClicked"][0]["Descrizione"]?>
                    </h2>
                    <h3 class="font-weight-bold mt-4">
                        <?php echo $templateParams["articleClicked"][0]["PrezzoUnitario"]?>€</h3>
                    <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"):?>
                    <h4><button class="btn btn-primary btn-dark btn-block my-5" id="shoppingCartMobile">Aggiungi al
                            carrello</button></h4>

                    <?php endif; ?>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade hide" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header font-weight-bold">
                            <?php if(!isset($_SESSION["tipo"])): ?>
                            <h5 class="modal-title" id="ModalLabel">Per aggiungere un articolo al carrello è necessario
                                effettuare il login.</h5>
                            <?php endif; ?>
                            <?php if(isset($_SESSION["tipo"]) && $dbh->getQuantityArticleInStock($articleClicked)[0]["Quantità"] != 0): ?>
                            <h5 class="modal-title" id="ModalLabel">Articolo aggiunto al carrello!</h5>
                            <?php endif; ?>
                            <?php if(isset($_SESSION["tipo"]) && $dbh->getQuantityArticleInStock($articleClicked)[0]["Quantità"] == 0): ?>
                            <h5 class="modal-title" id="ModalLabel">Articolo non disponibile!</h5>
                            <?php endif; ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"
                                id="CloseModal">Chiudi</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
// Bind to the submit event of our form
$(document).ready(function() {
    $("#CloseModal").click(function() {
        //$("#updateNumCart").load(location.href + " #updateNumCart");
        location.reload();

    });
    $("#shoppingCartMobile, #shoppingCartDesktop").click(function() {
        let code = <?php echo $templateParams["articleClicked"][0]["Codice"]; ?>;
        let action = 1 //add article
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                action: action
            },
            success: function(e) {
                $("#articleModal").modal("show");
            }
        });
    });
});
            </script>
            <div class="row">
                <footer class="page-footer bg-dark px-5 py-3 text-monospace text-left">
                    <p class="border-bottom pb-1"><a class="page-footer text-white" href="index.php">Borboni Luce
                            <img src="res/LogoMenu.png" class="img-responsive" width="15" height="20"
                                alt="Lampadina stilizzata logo Borboni Luce" loading="lazy" /></a></p>
                    <p class="border-bottom pb-1"><a class="page-footer text-white" href="index.php">Scopri
                            e acquista</a></p>
                    <p class="border-bottom pb-1"><a class="page-footer text-white" href="profile.php">Accedi</a>
                    </p>
                    <p class="border-bottom pb-1"><a class="page-footer text-white" href="map.php">Mappa del
                            sito</a>
                    </p>
                    <p class="border-bottom pb-1"><a class="page-footer text-white" href="Info.php">Info
                            su Borboni Luce</a></p>
                    <p class="page-footer text-white pb-2" style="font-size: 12px">*Borboni Luce S.r.l. donerà al
                        Fund
                        for the Global Fund (precedentemente noto come U.S. Fund for the Global Fund) 1 euro per
                        ogni
                        acquisto effettuato superiore ad una somma pari a 50 euro sul sito ufficiale qui presente.
                        Il
                        tetto massimo
                        per le donazioni è di un milione di dollari. Parte del ricavato dalla vendita dei pannelli
                        fotovoltaici viene devoluta al COVID-19 Response del Global Fund.</p>
                    <p class="page-footer text-white border-bottom pb-1" style="font-size: 12px">Per maggiori
                        informazioni chiama il numero: 800 533 229</p>
                    <p class="page-footer text-white" style="font-size: 12px">Copyright © 2020 Borboni Luce S.r.l.
                        Tutti
                        i diritti riservati - Italia</p>
                    <p class="page-footer text-white text-center pt-3" style="font-size: 12px">Norme sulla privacy |
                        Utilizzo dei cookie | Condizioni resi e rimborsi | Note legali </p>
                </footer>
            </div>
            </div>
    <div class="container-fluid">
        <div class="row bg-dark">
            <div class="col-5"></div>
            <div class="col-2 pt-2 text-center">
                <a href="index.php">
                    <img src="res/LogoMenu.png" class="img-responsive" width="25" height="40"
                        alt="Lampadina stilizzata logo Borboni Luce" loading="lazy" /></a>
            </div>
            <div class="col-5"></div>
        </div>

        <div class="row bg-dark">
            <div class="col-2 d-md-none">
                <a href="javascript: history.go(-1)" style="font-size: 24px; color: white;">
                    <em class="fas fa-arrow-left"></em>
                </a>
            </div>
            <div class="col-10"></div>
        </div>

        <div class="row bg-dark pb-5">
            <div class="col-3"></div>
            <div class="col-6 text-white text-center">
                <?php if (Title == "<title>Registrati</title>"): ?>
                <p style="font-size: 2.5em">Registrati</p>
                <?php else: ?>
                <p style="font-size: 2.5em">Accedi</p>
                <?php endif; ?>
            </div>
            <div class="col-3"></div>
        </div>
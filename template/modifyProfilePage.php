<div class="row">
    <div class="col-6 px-0 pt-3">
        <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
            <li>
                <p class="font-weight-bold" id="profileLab">
                    <em class="fas fa-user-circle"></em>
                    Profilo
                </p>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="image-area col-12 p-3 text-center">
        <?php if($profileVal[0]["Profilo"] == null): ?>
        <img id="imageResult" src="res/defaultProfileImg.png" class="img-fluid rounded-circle mx-auto d-block" />
        <?php else: ?>
        <img id="imageResult" src="res/<?php echo $profileVal[0]["Profilo"] ?>"
            class=" img-fluid rounded-circle mx-auto d-block" style="max-width: 256px; max-height: 256px;" />
        <?php endif; ?>
    </div>
</div>

<form method="post" role=" form">
    <div class="row">
        <div class="col-12 col-md-6 mx-auto text-center">
            <label class="font-weight-bold mr-1" for="uploadImg">Foto:</label>
            <input type="file" name="uploadImg" id="uploadImg" />
        </div>
    </div>
    <div class="row pt-3 px-5 py-4">
        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="nameInput">Nome</label>
            <input type="text" class="form-control" id="nameInput" name="name" aria-describedby="nameInput"
                value="<?php echo $profileVal[0]["Nome"] ?>" required />

        </div>

        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="surnameInput">Cognome</label>
            <input type="text" class="form-control" id="surnameInput" name="surname" aria-describedby="surnameInput"
                value="<?php echo $profileVal[0]["Cognome"] ?>" required />
        </div>

        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="birthInput">Data di nascita</label>
            <input type="date" class="form-control" id="birthInput" name="birthDate"
                value="<?php echo $profileVal[0]["DataDiNascita"] ?>" required />
        </div>


        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="cityInput">Città</label>
            <input type="text" class="form-control" id="cityInput" name="city"
                value="<?php echo $profileVal[0]["Città"] ?>" required />
        </div>

        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="addressInput">Via</label>
            <input type="text" class="form-control" id="addressInput" name="address"
                value="<?php echo $profileVal[0]["Via"] ?>" required />
        </div>

        <div class="col-12 col-md-1 py-3">
            <label class="font-weight-bold" for="civicInput">N. civico</label>
            <input type="number" min="0" class="form-control" id="civicInput" name="civic"
                value="<?php echo $profileVal[0]["NumeroCivico"] ?>" required />
        </div>
        <div class="col-12 col-md-5 py-3">
            <label class="font-weight-bold" for="phoneInput">Telefono (facoltativo)</label>
            <input type="tel" class="form-control" id="phoneInput" name="phone" pattern="[0-9]{10}"
                value="<?php echo $profileVal[0]["Telefono"] ?>">
        </div>

        <div class="col-12 col-md-6 py-3">
            <label class="font-weight-bold" for="societyInput">Azienda (facoltativo)</label>
            <input type="text" class="form-control" id="societyInput" name="company"
                value="<?php echo $profileVal[0]["Azienda"] ?>">
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-3"></div>
        <div class="col-12 col-md-3 my-1">
            <a class="btn btn-dark btn-block" id="undo" href="javascript: history.go(-1)">Annulla</a>
        </div>
        <div class="col-12 col-md-3 my-1">
            <input type="submit" class="btn btn-primary btn-block" id="save" value="Salva" />
        </div>
        <div class="col-md-3"></div>
    </div>
</form>

<script>
$("#uploadImg").change(function(e) {
    e.preventDefault();
    let dati = new FormData();
    dati.append("uploadImg", $("#uploadImg")[0].files[0], "profile.png");
    $.ajax({
        url: 'editProfile.php',
        data: dati,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    });
});



$("#save").click(function(e) {
    e.preventDefault();

    let email = '<?php echo $profileVal[0]["Email"]; ?>';

    let nameInput = document.getElementById("nameInput").value;
    let surnameInput = document.getElementById("surnameInput").value;
    let birthDateInput = document.getElementById("birthInput").value;
    let cityInput = document.getElementById("cityInput").value;
    let addressInput = document.getElementById("addressInput").value;
    let civicInput = document.getElementById("civicInput").value;
    let phoneNumberInput = document.getElementById("phoneInput").value;
    let society = document.getElementById("societyInput").value;

    let requiredFields = ["nameInput", "surnameInput", "birthInput", "cityInput", "addressInput", "civicInput"];
    let i = 0;
    for (i = 0; i < requiredFields.length; i++) {
        if (document.getElementById(requiredFields[i]).value == "") {
            document.getElementById(requiredFields[i]).className += " is-invalid";
            return false;
        }
    }

    let action = 1;
    $.ajax({
        url: "editProfile.php",
        type: "POST",
        data: {
            action: action,
            email: email,
            name: nameInput,
            surname: surnameInput,
            birthDate: birthDateInput,
            city: cityInput,
            address: addressInput,
            civic: civicInput,
            phoneNumber: phoneNumberInput,
            societyInput: society
        }
    }).done(function(e) {
        window.location.href = "profile.php";
    });
});
</script>
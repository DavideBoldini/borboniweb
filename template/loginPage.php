<div class="row">
    <div class="col-md-3"></div>
    <div class="col-12 col-md-6 pt-5 px-5">
        <h3 class="font-weight-bold">Inserire le credenziali:</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-12 col-md-6 px-5" id="divForm">
        <form id="loginForm">
            <div class="form-group">
                <label for="InputEmail"><strong>Email:</strong></label>
                <input type="email" name="email" class="form-control" id="InputEmail" placeholder="e-mail" required>
            </div>
            <div class="form-group">
                <label for="InputPassword"><strong>Password:</strong></label>
                <input type="password" name="password" class="form-control" id="InputPassword" placeholder="Password"
                    required>
            </div>
            <div id="errorLogin" class="invalid-feedback hide mb-1">
                Credenziali non valide
            </div>
            <input id="Accedi" type="submit" class="btn btn-primary btn-dark btn-lg btn-block" value="Accedi" />
        </form>

    </div>
</div>

<div class="row">
    <div class="col-12 border-bottom pt-4 pb-4 text-center">
        <a class="text-dark" href="register.php">Non hai un account? Registrati</a>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="loginModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Benvenuto in Borboni Luce!
                </h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>
                    Login effettuato con successo
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="okButton" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
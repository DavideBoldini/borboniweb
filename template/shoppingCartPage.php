<div class="row">
    <div class="col-12 px-0 pt-3">
        <aside>
            <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                <li>
                    <p class="font-weight-bold" id="faq">
                        &#128722; Carrello
                    </p>
                </li>
            </ul>
        </aside>
    </div>
</div>
<div class="container-fluid">
    <div class="row text-center d-md-none">
        <div class="col-12 mt-5">
            <h2 class="text-center font-weight-bold">Totale
                (<?php echo $dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"]?> articoli):
                <?php echo $dbh->getTotalAmount($_SESSION["email"])[0]["TOTALE"]?>€</h2>
        </div>
        <div class="col-12 col-md-6 mb-3 mx-auto">
            <a title="conferma ordine" href="<?php if($dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"]>0){
                        echo "ConfirmOrder.php";}?>">
            </a>
            <a type="submit" class="btn btn-primary btn-dark btn-block" id="confirmOrderMobile" href="<?php if($dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"]>0){
                        echo "ConfirmOrder.php";}?>">Procedi
                all'ordine</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-8">
            <?php foreach($templateParams["articoli_carrello"] as $articolocarrello): ?>

            <div class="row py-4 shadow p-3 my-4 bg-white rounded">
                <div class="col-6 col-md-6">
                    <img src="<?php echo IMG_DIR.$articolocarrello["Immagine"]?>" class="img-fluid" alt="..." />
                </div>
                <div class="col-6 col-md-6">
                    <p class="font-weight-bold" style="font-size:1.5em;"><?php echo $articolocarrello["Nome"]?></p>
                    <p class="font-weight-bold" style="font-size:1.5em;">
                        <?php echo $articolocarrello["PrezzoUnitario"]?>€</p>
                    <div class="row">
                        <div class="col-5 col-md-2 d-flex align-items-center mr-4">
                            <label for="Quantity<?php echo $articolocarrello["Codice"]?>">Quantità:</label>
                        </div>
                        <div class="col-6 col-md-2">
                            <input type="number" min="1" class="form-control px-0"
                                id="Quantity<?php echo $articolocarrello["Codice"]?>"
                                value="<?php echo $articolocarrello["Quantità"] ?>"
                                max="<?php echo $dbh->getQuantityArticleInStock($articolocarrello["Codice"])[0]["Quantità"] ?>" />
                        </div>
                    </div>
                    <div id="QuantityStock<?php echo $articolocarrello["Codice"]?>">
                        <?php if($articolocarrello["Quantità"] < $dbh->getQuantityArticleInStock($articolocarrello["Codice"])[0]["Quantità"]) { ?>
                        <div class="col-12 col-md-6 py-2 pl-1 mt-4 p-0 alert alert-success" role="alert">
                            Quantità disponibile:
                            <?php echo $dbh->getQuantityArticleInStock($articolocarrello["Codice"])[0]["Quantità"];?>
                        </div>
                        <?php } else { ?>
                        <div class="col-12 col-md-6 py-2 pl-1 mt-4 p-0 alert alert-warning" role="alert">
                            Quantità disponibile:
                            <?php echo $dbh->getQuantityArticleInStock($articolocarrello["Codice"])[0]["Quantità"];?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-md-6 pt-3 p-0">
                        <button class="btn btn-primary btn-dark btn-block"
                            id="Rimuovi<?php echo $articolocarrello["Codice"]?>">Rimuovi</button>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>

        <div class="col col-md-4 d-none d-sm-none d-md-block text-center position-sticky">
            <div class="col-12 my-5 " id="checkOrder">
                <h2 class="text-center font-weight-bold">Totale
                    (<?php echo $dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"]?> articoli):
                    <?php echo $dbh->getTotalAmount($_SESSION["email"])[0]["TOTALE"]?>€</h2>
                <a class="btn btn-primary btn-dark btn-block" id="confirmOrderDesktop" href="<?php if($dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"]>0){
                        echo "ConfirmOrder.php";}?>">
                    Procedi all'ordine
                </a>

            </div>
        </div>
    </div>
</div>
            <div class="row">
                <div class="col-12 px-0 pt-3">
                    <aside>
                        <ul class="border-bottom" style="list-style: none;">
                            <li>
                                <p class="font-weight-bold" id="prodottiEvidenza">
                                    &#11088; Prodotti in evidenza
                                </p>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 px-4">
                    <div id="slideProdottiEvidenza" class="carousel slide mx-2" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slideProdottiEvidenza" data-slide-to="0" class="active"></li>
                            <li data-target="#slideProdottiEvidenza" data-slide-to="1"></li>
                            <li data-target="#slideProdottiEvidenza" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach($templateParams["articoli_rilevanti"] as $articolirilevanti): ?>
                            <?php if($templateParams["articoli_rilevanti"][0]["Immagine"] == $articolirilevanti["Immagine"]) {
                                echo '<div class="carousel-item active">';
                                }
                                else{
                                    echo '<div class="carousel-item">';
                                }
                            ?>
                            <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            <a href="article.php?articleClicked=<?php echo $articolirilevanti["Codice"];?>">
                                <?php endif; ?>
                                <img src="<?php echo IMG_DIR.$articolirilevanti["Immagine"]?>" class="img-fluid"
                                    alt="Immagine di un articolo in evidenza" />
                                <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            </a><?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <a class="carousel-control-prev" href="#slideProdottiEvidenza" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slideProdottiEvidenza" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-12 px-0">
                    <aside class="mt-4">
                        <ul class="border-bottom" style="list-style: none;">
                            <li>
                                <p class="font-weight-bold" id="offerteNatalizie">
                                    &#127876; Offerte natalizie</p>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 px-4">
                    <div id="slideOfferteNatalizie" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slideOfferteNatalizie" data-slide-to="0" class="active"></li>
                            <li data-target="#slideOfferteNatalizie" data-slide-to="1"></li>
                            <li data-target="#slideOfferteNatalizie" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach($templateParams["articoli_natalizi"] as $articolonatalizio): ?>
                            <?php if($templateParams["articoli_natalizi"][0]["Immagine"] == $articolonatalizio["Immagine"]) {
                            echo '<div class="carousel-item active">';
                        }
                        else{
                            echo '<div class="carousel-item">';
                        }
                        ?>
                            <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            <a href="article.php?articleClicked=<?php echo $articolonatalizio["Codice"] ?>"><?php endif; ?>
                                <img src="<?php echo IMG_DIR.$articolonatalizio["Immagine"]?>" class="img-fluid"
                                    alt="Immagine di un articolo natalizio" />
                                <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            </a><?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <a class="carousel-control-prev" href="#slideOfferteNatalizie" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slideOfferteNatalizie" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-12 px-0">
                    <aside class="mt-4">
                        <ul class="border-bottom" style="list-style: none;">
                            <li>
                                <p class="font-weight-bold" id="nuoviArrivi">
                                    <em class="fas fa-tags"></em>
                                    Nuovi arrivi
                                </p>

                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 px-4 pb-5">
                    <div id="slideNuoviArrivi" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slideNuoviArrivi" data-slide-to="0" class="active"></li>
                            <li data-target="#slideNuoviArrivi" data-slide-to="1"></li>
                            <li data-target="#slideNuoviArrivi" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <?php foreach($templateParams["articoli_nuovi"] as $articolinuovi): ?>
                            <?php if($templateParams["articoli_nuovi"][0]["Immagine"] == $articolinuovi["Immagine"]) {
                                echo '<div class="carousel-item active">';
                                }
                                else{
                                    echo '<div class="carousel-item">';
                                }
                            ?>
                            <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            <a href="article.php?articleClicked=<?php echo $articolinuovi["Codice"] ?>"><?php endif; ?>
                                <img src="<?php echo IMG_DIR.$articolinuovi["Immagine"]?>" class="img-fluid"
                                    alt="Immagine di un nuovo prodotto" />
                                <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin"): ?>
                            </a><?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <a class="carousel-control-prev" href="#slideNuoviArrivi" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slideNuoviArrivi" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            </div>
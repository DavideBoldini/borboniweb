<!DOCTYPE html>
<html lang="it">
    <?php require($templateParams["head"]); ?>

    <body class="bg-light">
        <?php if(Title == "<title>Registrati</title>" || Title == "<title>Accedi</title>"){
            require($templateParams["headerLR"]);
        }else{
            require($templateParams["header"]);
        }
        ?>
        <?php require($templateParams["body"]); ?>
        <?php require($templateParams["footer"]); ?>

    </body>

</html>
<div class="collapse col-12 pt-2" id="alertDiv">
    <div class="col-12 alert alert-success text-center mx-auto" role="alert">
        <h4 class="alert-heading">Ordine eseguito con successo!</h4>
        <p>Grazie per l'acquisto!</p>
        <div class="col-12 col-md-6 mx-auto">
            <button type="button" class="btn btn btn-success btn-block" id="backHome">Torna alla home</button>
        </div>
    </div>
</div>

<div id="toPrint">
    <div class="row">
        <div class="col-12 text-center pt-3">
            <h1 class="font-weight-bold">Riepilogo ordine:</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12 p-4">
            <fieldset class="form-group">
                <legend class="font-weight-bold"><u>Articoli:</u></legend>
                <div class="table-responsive table-striped table-hover">
                    <table class="table mt-3">
                        <thead class="thead-dark">
                            <tr>
                                <th id="codice" scope="col">Codice Articolo</th>
                                <th id="nome" scope="col">Nome</th>
                                <th id="prezzoUnitario" scope="col">Prezzo Unitario</th>
                                <th id="quantità" scope="col">Quantità</th>
                                <th id="tipo" scope="col">Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($_SESSION["articlesInCart"] as $articolocarrello): ?>
                            <tr>
                                <th scope="row" id="codiceArticolo"><?php echo $articolocarrello["Codice"]?></th>
                                <td headers="nome articolo"><?php echo $articolocarrello["Nome"]?></td>
                                <td headers="prezzo articolo"><?php echo $articolocarrello["PrezzoUnitario"]?></td>
                                <td headers="quantità articolo"><?php echo $articolocarrello["Quantità"]?></td>
                                <td headers="tipo articolo"><?php echo $articolocarrello["Tipo"]?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

            </fieldset>
        </div>
        <div class="col-12 p-4">
            <fieldset class="form-group" disabled>
                <legend class="font-weight-bold"><u>Dettagli:</u></legend>
                <div class="col-12 col-md-6 py-3">
                    <label class="font-weight-bold" for="numArticles">Numero Articoli</label>
                    <input type="text" id="numArticles" class="form-control"
                        placeholder="<?php echo $templateParams["articlesNumber"]?>">
                </div>


            </fieldset>
        </div>

        <div class="col-12 col-md-6 p-4">
            <fieldset class="form-group">
                <legend class="font-weight-bold"><u>Dati di spedizione:</u></legend>
                <div class="col-12 col-md-6 py-2">
                    <label class="font-weight-bold" for="cityInput">Città</label>
                    <input type="text" class="form-control" id="cityInput" name="city" aria-describedby="cityInput"
                        value="<?php echo $profileVal[0]["Città"]?>" required>
                </div>

                <div class="col-12 col-md-6 py-2">
                    <label class="font-weight-bold" for="addressInput">Via</label>
                    <input type="text" class="form-control" id="addressInput" name="address"
                        aria-describedby="addressInput" value="<?php echo $profileVal[0]["Via"]?>" required>
                </div>

                <div class="col-12 col-md-6 py-2">
                    <label class="font-weight-bold" for="civicInput">N. civico</label>
                    <input type="number" min="0" class="form-control" id="civicInput" name="civic"
                        aria-describedby="civicInput" value="<?php echo $profileVal[0]["NumeroCivico"]?>" required>
                </div>

            </fieldset>
        </div>
        <div class="col-12 col-md-6 p-4">
            <fieldset class="form-group">
                <legend class="font-weight-bold"><u>Dati Personali:</u></legend>
                <div class="col-12">
                    <label class="font-weight-bold">Nome</label>
                    <p><?php echo $profileVal[0]["Nome"]?></p>
                </div>

                <div class="col-12">
                    <label class="font-weight-bold">Cognome</label>
                    <p><?php echo $profileVal[0]["Cognome"]?></p>
                </div>
                <div class="col-12">
                    <label class="font-weight-bold">Email</label>
                    <p><?php echo $profileVal[0]["Email"]?></p>
                </div>
            </fieldset>
        </div>
        <div class="col-12 p-4 text-center">
            <h2 class="font-weight-bold">Totale: <?php echo $templateParams["totalAmount"]?>€</h2>
        </div>

    </div>
</div>

<div class="col-12 col-md-6 mx-auto py-3 text-center">
    <label for="ConfirmOrderButton"></label>
    <input id="ConfirmOrderButton" type="submit" class="btn btn-success btn-block" value="Conferma e Salva"
        data-toggle="collapse" data-target="#alertDiv" aria-expanded="false" aria-controls="alertDiv" />
</div>
<div class="col-12 col-md-6 mx-auto pb-4 text-center">
    <button type="button" class="btn btn-primary btn-dark btn-block" id="undo">Annulla</button>
</div>

<script>
window.onbeforeunload = function() {
    return false;
}
</script>
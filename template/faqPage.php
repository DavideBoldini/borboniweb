<div class="row">
    <div class="col-12 px-0 pt-3">
        <aside>
            <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                <li>
                    <p class="font-weight-bold" id="faq">
                        &#10067; FAQ
                    </p>
                </li>
            </ul>
        </aside>
    </div>
</div>

<div class="accordion md-accordion mb-3 mx-2" id="accordionEx1" role="tablist" aria-multiselectable="true">

    <div class="card">

        <div class="card-header" role="tab" id="headingTwo1">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                aria-expanded="false" aria-controls="collapseTwo1">
                <h5 class="mb-0">
                    Pagamento e fatturazione
                </h5>
            </a>
        </div>

        <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
            data-parent="#accordionEx1">
            <div class="card-body">
                <p class="px-3" style="font-weight: bold;"><strong>Quali sono le tipologie di pagamento?</strong></p>
                <p class=" px-3">Le modalità di pagamento sono molteplici:</p>
                <ul>
                    <li>Carte di credito e prepagate dei circuiti VISA, VISA Electron (come la PostePay), American
                        Express e Mastercard;</li>
                    <li>Paypal</li>
                    <li>Contanti alla consegna dei prodotti</li>
                    <li>Assegni e bonifici per ordini superiori a 500 euro.</li>
                </ul>
                <p class="border-bottom pb-2 px-3"> </p>
                <p class="px-3" style="font-weight: bold;"><strong>Mi sono dimenticato di richiedere la fattura. Come
                        posso
                        fare?</strong></p>
                <p class="border-bottom pb-2 px-3">L'acquirente non deve preoccuparsi, poichè la fattura sarà emessa in
                    automatico e la si riceverà entro 15 giorni.</p>
                <p class="px-3"><strong>Ho cambiato idea su un ordine non ancora consegnato. Posso
                        modificarlo?</strong></p>
                <p class="border-bottom pb-2 px-3">Non esitare a contattarci via telefonica/posta elettronica per
                    entrare nel merito della tua specifica situazione. In linea generale è sempre possibile modificare
                    un ordine, ma per ordini superiori a 500 euro, si necessita un preavviso
                    di almeno due settimane rispetto alla data di consegna.</p>
                <p class="px-3"><strong> Come posso verificare lo stato dell'ordine?</strong></p>
                <p class="px-3"> Le ricordiamo che nell'area clienti è presente una sezione "Stato Ordine", in cui poter
                    inserire il numero dell'ordine e verificarne lo stato.</p>
            </div>
        </div>

    </div>

    <div class="card">

        <div class="card-header" role="tab" id="headingTwo21">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo21"
                aria-expanded="false" aria-controls="collapseTwo21">
                <h5 class="mb-0">
                    Prodotti e disponibilità
                </h5>
            </a>
        </div>

        <div id="collapseTwo21" class="collapse" role="tabpanel" aria-labelledby="headingTwo21"
            data-parent="#accordionEx1">
            <div class="card-body">
                <p class="px-3"><strong>Il prodotto che mi interessava è esaurito, quanto tempo
                        occorre aspettare per il rifornimento?</strong></p>
                <p class="border-bottom pb-2 px-3">Mediamente 2 settimane, ma per prodotti con una richiesta molto
                    elevata si può trattare anche di pochi giorni.</p>
                <p class="px-3"><strong>Dove trovo tutte le informazioni per il
                        montaggio?</strong></p>
                <p class="border-bottom pb-2 px-3">All'interno dell'imballaggio vi è presente una guida per il corretto
                    montaggio delle luci, in caso sia assente ci scusiamo e la invitiamo a contattarci al più presto.
                </p>
                <p class="px-3"><strong>Ho bisogno di un pezzo di ricambio per la luce
                        acquistata.
                        Come posso fare?</strong></p>
                <p class="border-bottom pb-2 px-3"> Le ricordiamo che i nostri prodotti hanno una garanzia valida per un
                    anno, quindi nel caso in cui il prodotto fosse ancora in garanzia, le consigliamo di
                    scriverci/contattarci telefonicamente per la sostituzione dell'articolo.
                </p>
                <p class="px-3" style="font-weight: bold;"><strong>Un articolo che ho acquistato presenta dei problemi,
                        cosa
                        posso fare?</strong></p>
                <p class="border-bottom pb-2 px-3">La invitiamo a contattarci per esporre la problematica riscontrata,
                    in modo da poter escludere errori di montaggio. Nel caso in cui fosse un difetto di produzione,
                    procederemo immediatamente con la sostituzione dell'articolo.</p>
                <p class="px-3" style="font-weight: bold;"><strong>Quali sono i termini di garanzia previsti per i
                        prodotti
                        Borboni Luce?</strong></p>
                <p class="px-3">I nostri clienti rappresentano un patrimonio inestimabile, proprio per questo è prevista
                    una garanzia per qualsiasi tipo di danno per un anno, superato l'anno, sarà presente una garanzia
                    contro i danni ambientali con terminazione
                    al terzo anno dall'acquisto del prodotto.</p>
            </div>
        </div>

    </div>

    <div class="card">

        <div class="card-header" role="tab" id="headingThree31">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseThree31"
                aria-expanded="false" aria-controls="collapseThree31">
                <h5 class="mb-0">
                    Trasporto ed impianto
                </h5>
            </a>
        </div>

        <div id="collapseThree31" class="collapse" role="tabpanel" aria-labelledby="headingThree31"
            data-parent="#accordionEx1">
            <div class="card-body">
                <p class="px-3"><strong>Il trasporto quanto tempo impiega generalmente?</strong></p>
                <p class="border-bottom pb-2 px-3">Il tempo può variare da Regione a Regione, ma in linea di
                    massima la
                    consegna è prevista fra i 3 e 5 giorni dalla conferma dell'ordine.</p>
                <p class="px-3"><strong>L'impianto per ordini di grandi dimensioni avviene
                        autonomamente?</strong></p>
                <p class="border-bottom pb-2 px-3">Per ordini superiori a 500 euro, l'impianto ed il montaggio verrà
                    effettuato dai nostri collaboratori.</p>
                <p class="px-3"><strong>Posso chiedere di consegnare un ordine all'estero?</strong></p>
                <p class="border-bottom pb-2 px-3">Siamo spiacenti, ma le consegne vengono effettuate solo all'interno
                    del territorio italiano.</p>
                <p class="px-3"><strong>La mia consegna è in ritardo/non ho ricevuto il mio
                        ordine.
                        Cosa devo fare?</strong></p>
                <p class="px-3">La preghiamo di contattarci immediatamente fornendoci tutti i dati necessari per
                    l'identificazione dell'ordine.</p>
            </div>
        </div>

    </div>
    <div class="card">

        <div class="card-header" role="tab" id="headingFour41">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseFour41"
                aria-expanded="false" aria-controls="collapseFour41">
                <h5 class="mb-0">
                    Resi, reclami e rimborsi
                </h5>
            </a>
        </div>

        <div id="collapseFour41" class="collapse" role="tabpanel" aria-labelledby="headingFour41"
            data-parent="#accordionEx1">
            <div class="card-body">
                <p class="px-3"><strong>Ho cambiato idea su un prodotto acquistato/voglio fare un
                        reso. Cosa posso fare?</strong></p>
                <p class="border-bottom pb-2 px-3">Hai acquistato un prodotto ma hai cambiato idea? Nessun problema:
                    puoi rendere il prodotto entro 365 giorni dalla data di acquisto. Puoi anche richiedere il ritiro
                    dei tuoi articoli a domicilio: in questo caso le spese di trasporto
                    per la restituzione saranno a tuo carico.</p>
                <p class="px-3"><strong>Ho cambiato idea su un prodotto. Posso richiederne il ritiro
                        a domicilio?</strong></p>
                <p class="border-bottom pb-2 px-3">Se vuoi usufruire del Cambio Idea puoi acquistare il ritiro a
                    domicilio del prodotto che non desideri più. Il costo del servizio parte da € 4,90 per ordini fino a
                    50 kg e da € 59 per ordini superiori a 50 kg. Il pagamento avviene
                    al ritiro del prodotto.</p>
                <p class="px-3"><strong>Ho ricevuto un articolo danneggiato/diverso da quello
                        ordinato. Come devo comportarmi?</strong></p>
                <p class="border-bottom pb-2 px-3">La preghiamo di contattarci immediatamente per la sostituzione
                    del/dei prodotto/i danneggiati.</p>
                <p class="px-3" style="font-weight: bold;"><strong>L'articolo ricevuto non rispecchia le mie necessità,
                        è
                        possibile ottenere un rimborso?</strong></p>
                <p class="px-3">Certamente è possibile ricevere un rimborso nel caso in cui si rientri nei 90 giorni
                    dalla consegna del prodotto. La invitiamo a contattarci per capire in che modo il prodotto non
                    rispecchia le sue esigenze, poichè essendo il nostro
                    catalogo ben fornito, siamo certi di trovare il prodotto che fa per lei.</p>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-dark bg-dark fixed-top px-0" role="navigation">
    <div class="container-fluid px-1">
        <div class="col-1 d-md-none d-lg-none d-xl-none px-1">
            <button class="navbar-toggler border-dark p-0" type="button" data-toggle="collapse"
                data-target="#navbarToggleMenu" aria-controls="navbarToggleMenu" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="col-6 col-md-2 text-center">
            <a class="navbar-brand m-0" href="index.php">Borboni Luce
                <img src="res/LogoMenu.png" class="img-responsive" width="30" height="40"
                    alt="Lampadina stilizzata logo Borboni Luce" loading="lazy" role="presentation" />
            </a>
        </div>
        <div class="col-md-7 d-none d-sm-none d-md-block" role="search">
            <div class="md-form">
                <form class="form-inline d-flex justify-content-center md-form form-sm mt-0" method="get"
                    action="searchArticle.php">
                    <label for="searchBarDesktop">
                        <button type="submit" style="background:none;border:none;padding:0;">
                            <em class="fas fa-search text-white"></em>
                        </button>
                    </label>
                    <input title="barra di ricerca per desktop" class="form-control form-control-sm ml-3 w-75"
                        type="text" id="searchBarDesktop" name="articleName" placeholder="Cerca" aria-label="Search" />
                </form>
            </div>
        </div>

        <div class="col-1 col-md-1" id="countNotifications">
            <div class="navbar-brand m-0" id="notif">
                <button class="btn btn-dark" data-toggle="collapse" type="button" aria-expanded="false"
                    data-target="#navbarToggleNotif" style="background:none;border:none;padding:0;font-size:1.0em">
                    <em class="fas fa-bell"></em>
                    <?php 
                    if(isset($_SESSION["email"])):
                        if(count($dbh->getNotifications($_SESSION["email"])) > 0) :?>
                    <span class='badge badge-warning' id="updateNotif"><?php echo count($dbh->getNotifications($_SESSION["email"])) ?></span>
                    <?php endif; 
                    endif;?>
                    <!-- NOTA: 
                        Lato desktop -> Campanella + num notifiche (se presenti)
                        Lato mobile -> Campanella sostituita da numero
                    -->
                </button>
            </div>
            <script>
            $(document).ready(function() {
                $('.toast').toast('show');
            });
            </script>
        </div>
        <div class="col-1 col-md-1">
            <a class="navbar-brand m-0" href="
                <?php if(!isset($_SESSION["email"])){
                    echo "login.php";   
                }else{
                    echo "profile.php";
                }
            ?>">
                <em class="fas fa-user"></em>
            </a>
        </div>
        <div class="col-1 col-md-1" id="updateNumCart">
            <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin"): ?>
            <a class="navbar-brand m-0" href="stock.php">
                <em class="fas fa-warehouse"></em>
            </a>
            <?php else: ?>
            <a class="navbar-brand m-0" href="shoppingCart.php">
                <em class="fas fa-shopping-cart"></em>
                <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "Utente" && $dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"] > 0): ?>
                <span class='badge badge-warning' id='lblCartCount'><?php echo $dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"] ?></span>
                <?php endif; ?>
            </a>
            <?php endif; ?>
        </div>
        <div class="col-1"></div>
    </div>
</nav>
<nav class="navbar d-none d-sm-none d-md-block bg-secondary px-0 py-3" role="navigation">
    <div class="container-fluid">
        <div class="col-1"></div>
        <div class="col-md-2">
            <a class="text-white" href="catalogInt.php">Interni</a>
        </div>
        <div class="col-md-2">
            <a class="text-white" href="catalogExt.php">Esterni</a>
        </div>
        <div class="col-md-2">
            <a class="text-white" href="catalogPanel.php">Pannelli Solari</a>
        </div>
        <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin") { ?>
        <div class="col-md-2">
            <a class="text-white" href="sales.php">Vendite</a>
        </div>
        <?php } else {?>
        <div class="col-md-2">
            <a class="text-white" href="faq.php">FAQ</a>
        </div>
        <?php } ?>
        <div class="col-md-2">
            <a class="text-white" href="info.php">Chi siamo</a>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="collapse navbar-collapse bg-dark px-3" id="navbarToggleMenu">
        <div class="row">
            <div class="col-12 py-3 border-bottom">
                <div class="md-form">
                    <form class="form-inline d-flex justify-content-center md-form form-sm mt-0" method="get"
                        action="searchArticle.php">
                        <label for="searchBarMobile">
                            <button type="submit" style="background:none;border:none;padding:0;">
                                <em class="fas fa-search text-white" aria-hidden="true"></em>
                            </button>
                        </label>
                        <input title="barra di ricerca per mobile" class="form-control form-control-sm ml-3 w-75"
                            id="searchBarMobile" type="text" placeholder="Cerca" name="articleName" aria-label="Search">
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="">Home </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-3">
                <p class="text-white font-weight-bold">Prodotti:</p>
            </div>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col pt-0">
                <a class="text-white" href="catalogInt.php">Interno </a>
            </div>
        </div>
        <div class="row">
            <div class="col-1"></div>
            <div class="col pt-3">
                <a class="text-white" href="catalogExt.php">Esterno </a>
            </div>
        </div>
        <div class="row border-bottom">
            <div class="col-1"></div>
            <div class="col py-3">
                <a class="text-white" href="catalogPanel.php">Pannelli solari</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="
                <?php if(!isset($_SESSION["email"])){
                    echo "login.php";   
                }else{
                    echo "profile.php";
                }
                ?>">
                    Account
                </a>
            </div>
        </div>
        <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin"): ?>
        <div class="row">
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="sales.php">Vendite </a>
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
            <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin") { ?>
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="stock.php">Giacenza </a>
            </div>
            <?php } else { ?>
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="shoppingCart.php">Carrello </a>
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="faq.php">FAQ </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 p-3 border-bottom">
                <a class="text-white font-weight-bold" href="info.php">Chi siamo </a>
            </div>
        </div>
    </div>

    <?php if(isset($_SESSION["email"])): ?>
    <div class="collapse navbar-collapse bg-dark px-3" id="navbarToggleNotif">
        <div class="row py-3 border-bottom">
            <div class="col-12 text-center">
                <p class="text-light font-weight-bold" style="font-size: 1.6em">Notifiche</p>
            </div>
            <div class="col-12">
                <button class="btn btn-danger btn-block" id="deleteAllNotifications">Cancella tutto</button>
            </div>
        </div>

        <div class="row">
            <div class="overflow-auto" style="max-width: 100vh; max-height: 75vh;" id="notificationList">
                <?php foreach($dbh->getNotifications($_SESSION["email"]) as $notifica) :?>
                <div class="col-12 my-2" id="notificItem<?php echo $notifica["Codice"]?>">
                    <div class="toast" data-autohide="false" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <div class="col-12 pl-0 pr-1">
                                <div class="col-md-12 col-12 p-0">
                                    &#128276;
                                    <small
                                        class="col-md-3 col-12 p-0"><?php echo date_format(date_create($notifica["Data"]),'d/m'); echo " ore "; echo $notifica["Orario"]; ?></small>
                                    <button type="button" class="mb-1 close col-1" aria-label="Close"
                                        id="chiudiNotifica<?php echo $notifica["Codice"]?>">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <strong class="col-md-9 col-12 p-0">
                                    <?php echo $notifica["Titolo"] ?></strong>
                            </div>
                        </div> <!-- data-dismiss="toast" -->
                        <div class="toast-body">
                            <?php echo $notifica["Descrizione"] ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php endif;?>

    <script>
    // Bind to the submit event of our form
    $('button[id^="chiudiNotifica"]').click(function() {
        const action = 10; //delete notification
        const code = $(this).attr('id').substring(14);
        const codice = "notificItem" + code;
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                action: action
            }
        }).done(function(data) {
            $("#" + codice).remove();
            $("#notif").load(location.href + " #notif");
        });
    });

    $("#deleteAllNotifications").click(function() {
        const action = 11;
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                action: action
            }
        }).done(function(data) {
            $('button[id^="notificItem"]').remove();

            $("#notificationList").load(location.href + " #notificationList");
            $("#notif").load(location.href + " #notif");
        });
    });
    </script>
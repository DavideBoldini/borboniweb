            <div class="row">
                <div class="col-12 px-0 pt-3">
                    <aside>
                        <ul class="border-bottom font-weight-bold" style="list-style: none; font-size: 1.5em">
                            <li>
                                <em class="fas fa-pen"></em> Modifica articolo
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-12 col-md-5 mt-3 py-3 px-3 text-center">
                    <img src="<?php echo IMG_DIR.$templateParams["articleToUpdate"][0]["Immagine"]?>" class="img-fluid"
                        alt="immagine dell'articolo" required />
                </div>
                <div class="col-12 col-md-6 font-weight-bold mt-5">
                    <div class="mr-3 mb-2">
                        <label class="font-weight-bold" for="updatedName">Nome Articolo</label>
                        <input type="text" class="form-control" id="updatedName"
                            value="<?php echo $templateParams["articleToUpdate"][0]["Nome"]?>" required />
                    </div>
                    <div class="mr-3 mb-2">
                        <label class="font-weight-bold" for="updateDesc">Descrizione</label>
                        <textarea class="form-control" id="updateDesc" name="updateDesc" rows="3"
                            required><?php echo $templateParams["articleToUpdate"][0]["Descrizione"]?></textarea>
                    </div>
                    <div class="mr-3 mb-2">
                        <label class="font-weight-bold" for="updatePrice">Prezzo Unitario</label>
                        <input type="number" class="form-control" id="updatePrice"
                            value="<?php echo $templateParams["articleToUpdate"][0]["PrezzoUnitario"]?>" step=".01" min="0" required> 
                    </div>
                    <div>
                        <input type="submit" class="btn btn-primary mt-2 p-2 mr-4" id="save" value="Salva" />
                        <a href="javascript: history.go(-1)" class="btn btn-primary btn-dark mt-2 p-2"
                            id="undo">Annulla</a>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade hide" id="articleModal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Articolo modificato correttamente!</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-footer">
                            <a href="javascript: history.go(-1)" class="btn btn-primary">OK</a>
                        </div>
                    </div>
                </div>
            </div>
            <script>
// Bind to the submit event of our form
$("#save").click(function() {
    let code = <?php echo $articleToUpdate; ?>;
    let action = 5 //update article
    let newName = document.getElementById("updatedName").value;
    let newDesc = document.getElementById("updateDesc").value;
    let newPrice = document.getElementById("updatePrice").value;

    let requiredFields = ["updatedName", "updateDesc", "updatePrice"];
    let i = 0;
    for (i = 0; i < requiredFields.length; i++) {
        if (document.getElementById(requiredFields[i]).value == "") {
            document.getElementById(requiredFields[i]).className += " is-invalid";
            return false;
        }
    }
    $.ajax({
        url: "operationCases.php",
        type: "POST",
        data: {
            code: code,
            action: action,
            newName: newName,
            newDesc: newDesc,
            newPrice: newPrice
        },
        success: function(e) {
            $("#articleModal").modal({
                backdrop: "static",
                keyboard: false
            });
            $("#articleModal").modal("show");
        }
    });
});
            </script>
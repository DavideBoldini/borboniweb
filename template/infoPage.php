<div class="row">
    <div class="col-12 px-0 pt-3">
        <aside>
            <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                <li>
                    <p class="font-weight-bold" id="chiSiamo">
                        &#127970; Chi siamo
                    </p>
                </li>
            </ul>
        </aside>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> ORIGINI </h2>
        <p class="border-bottom pb-1 px-3"> Siamo negli anni ‘60 e una piccola azienda artigianale realizza le prime
            insegne luminose in materiale plastico per hotel, bar e ristoranti nella Riviera Adriatica che, in quegli
            anni, sta vivendo un vero e proprio boom economico. Assecondando
            le specifiche richieste della clientela di questa regione costiera, e cercando di trasformare un’attività
            esclusivamente estiva in un impegno per tutto l’anno, l’azienda inizia a costruire i primi semplici
            apparecchi d'illuminazione
            sfruttando tutta l’esperienza maturata. Il grande successo ottenuto da questi primi prodotti induce Franco
            Calabria, ideatore e proprietario, a dedicarsi esclusivamente alla costruzione di apparecchi
            d'illuminazione... è il 1973 e Borboni
            Luce è fondata. Dall’uso razionale della plastica all’adozione di materiali inediti, dalla lotta contro
            l’inquinamento visivo ad una sempre più viva sensibilità sociale, dal continuo aggiornamento tecnologico
            alla collaborazione con
            affermati designer: la nostra ricerca ci spinge a non accontentarci mai dei risultati raggiunti e ad
            esplorare sempre nuovi territori. Illuminare è il nostro mestiere. Lo facciamo con passione e competenza.
            Dal 1973.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> MISSION </h2>
        <p class="border-bottom pb-1 px-3"> Vogliamo che le nostre luci non tolgano mai all’ uomo il piacere di
            osservare il cielo stellato. Quanti apparecchi e sistemi di illuminazione abbiamo realizzato fino ad oggi,
            per case, città, piazze, strade, cortili, porticati, giardini,
            pavimenti, soffitti, capannoni, negozi e showroom? Abbiamo perso il conto. Quello che sappiamo bene è che
            ogni ambiente è sempre un contenitore di emozioni e sensazioni, da percepire e assaporare, anche grazie al
            lavoro della luce.
            Al talento e alla creatività degli architetti e dei progettisti offriamo oggi una gamma articolata e
            completa di apparecchi e accessori che reinterpretano lo spazio, interno od esterno, alla luce del design.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> QUALITÁ </h2>
        <p class="border-bottom pb-1 px-3"> Nel tempo, siamo cresciuti. Molto. Abbiamo aggiornato la nostra tecnologia,
            i nostri processi di lavorazione, la nostra gamma produttiva. Non abbiamo mai dimenticato, però, la nostra
            natura autenticamente artigiana che ci spinge ostinatamente
            a cercare sempre la massima qualità. La nostra qualità è ora riconosciuta dai più importanti enti
            internazionali di certificazione, ma, prima ancora, è apprezzata dai nostri clienti: giudici attenti e
            rigorosi.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> LE PERSONE </h2>
        <p class="border-bottom pb-1 px-3"> Dire che le imprese sono fatte di persone, oggi, è anche un po’
            un’abitudine, che non sempre corrisponde alla realtà. Per noi di BorboniLuce, si tratta invece una verità
            fondamentale, che sta alla radice del nostro stesso modo di “essere
            azienda”. Non abbiamo avuto bisogno di copiare l’eccellenza degli altri. Lavorare assieme, fare squadra, a
            tutti i livelli, ci è sempre venuto naturale. Per questo siamo grati a tutti i nostri collaboratori. Senza
            il loro apporto e
            il loro entusiasmo magari ce l’avremmo fatta ugualmente. Ma non sarebbe stata la stessa cosa.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> RESPONSABILITÁ </h2>
        <p class="border-bottom pb-1 px-3"> Le aziende non vivono sulla luna. Operano all’interno di un territorio e di
            un tessuto sociale dai quali ricevono almeno quanto restituiscono in termini di occupazione e crescita
            economica. Per questo progettiamo il nostro sviluppo in
            sintonia con il nostro ambiente e con le persone che vi abitano. Per questo diamo a chi è meno fortunato
            l’opportunità di inserirsi socialmente e di acquisire un lavoro. In collaborazione con le amministrazioni
            locali, inseriamo nel
            nostro organico lavoratori diversamente abili e detenuti, cui offriamo un’importante occasione di riscatto
            sociale e la possibilità di integrarsi. È un piccolo, grande progetto di cui noi e tutti i nostri
            collaboratori siamo profondamente
            orgogliosi.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-12 pt-3">
        <h2 class="pb-1 px-3" style="font-family:Georgia, 'Times New Roman', Times, serif;"> CERTIFICAZIONI </h2>
        <p class="border-bottom pb-1 px-3"> Abbiamo puntato tutto sulla qualità. E intendiamo non solo qualità del
            prodotto, ma anche della produzione, del servizio, del marketing. In una azienda moderna tutto deve essere
            di qualità e per questo motivo abbiamo lavorato a lungo,
            credendoci fino in fondo nonostante la difficoltà e le tentazioni di scelte più facili. Nel dicembre 1996
            abbiamo ottenuto il marchio UNI-EN ISO 9002 (ora convertito in ISO 9001:2008), un riconoscimento per quanto
            già realizzato ed
            un impegno per il futuro.
        </p>
    </div>
</div>
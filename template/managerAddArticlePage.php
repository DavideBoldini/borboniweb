<div class="row">
    <div class="col-12 pt-3">
        <aside>
            <ul class="border-bottom font-weight-bold p-1" style="list-style: none; font-size: 1.5em">
                <li class="my-1">
                    <em class="fas fa-plus-circle"></em> Aggiungi articolo
                </li>
            </ul>
        </aside>
    </div>
</div>

<form action="#" id="formArticle">
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="image-area my-3"><img id="imageResult" src="#"
                    class="img-fluid rounded shadow-sm mx-auto d-block" /></div>
            <label class="font-weight-bold mr-1" for="uploadImg">Immagine Articolo: </label>
            <input type="file" name="uploadImg" id="uploadImg" required />
        </div>
        <div class="col-12 col-md-6 mb-3 font-weight-bold">
            <div class="mr-3 mb-2">
                <label class="font-weight-bold" for="addName">Nome Articolo:</label>
                <input type="text" class="form-control" name="addName" id="addName" required />
            </div>
            <div class=" mr-3 mb-2">
                <label class="font-weight-bold" for="addDesc">Descrizione:</label>
                <textarea class="form-control" name="addDesc" id="addDesc" rows="3" required></textarea>
            </div>
            <div class="row mr-1">
                <div class="col-md-6">
                    <label class="font-weight-bold" for="addPrice">Prezzo Unitario:</label>
                    <input type="number" class="form-control" name="addPrice" id="addPrice" step=".01" min="0"
                        required />
                </div>
                <div class=" col-md-6">
                    <label for="tipoCatalogo">Tipo:</label>
                    <select class="custom-select" name="addType" title="Scegli un tipo" id="tipoCatalogo">
                        <option value="Interni" id="catalogInterni">Interni</option>
                        <option value="Esterni" id="catalogoEsterni">Esterni</option>
                        <option value="Pannelli Solari" id="catalogoPanSolari">Pannelli Solari</option>
                    </select>
                </div>
            </div>
            <div class="mb-2">
                <a class="btn btn-dark mt-5" id="undo" href="javascript: history.go(-1)">Annulla</a>
                <input type="submit" class="btn btn-primary ml-3 mt-5" id="add" value="Aggiungi" />
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade hide" id="articleModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Articolo inserito correttamente!</h5>
            </div>
            <div class="modal-footer">
                <a href="javascript: history.go(-1)" class="btn btn-primary">Chiudi</a>
            </div>
        </div>
    </div>
</div>
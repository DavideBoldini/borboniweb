<div class="row">
    <div class="col-12 px-0 pt-3">
        <aside>
            <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                <li>
                    <p class="font-weight-bold" id="faq">
                        &#128230; Giacenza
                    </p>
                </li>
            </ul>
        </aside>
    </div>
</div>

<form class="form-inline d-flex justify-content-center md-form form-sm mt-0" method="get" action="stock.php">
    <div class="col-12 col-md-9 py-3 text-center">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-1 col-md-1 p-0 align-self-center">
                <button class="p-0" type="submit" style="background:none;border:none;">
                    <em class="fas fa-search text-black" aria-hidden="true"></em>
                </button>

            </div>
            <div class="col-9 col-md-8">
                <label for="searchArticle"></label>
                <input title="inserire codice articolo" class="form-control w-100" id="searchArticle" type="text"
                    placeholder="Inserire codice articolo" name="stockArticleCode" aria-label="Search" />
            </div>
        </div>
    </div>
    <div class="col-9 col-md-2 py-3 mx-auto">
        <button type="submit" class="btn btn-primary btn-block">VISUALIZZA TUTTO</button>
    </div>
</form>

<div class="table-responsive table-striped table-hover">
    <table class="table mt-3 align-middle">
        <thead class="thead-dark">
            <tr>
                <th id="codice" scope="col">Codice Articolo</th>
                <th id="nome" scope="col">Nome</th>
                <th id="prezzoUnitario" scope="col">Prezzo Unitario</th>
                <th id="quantità" scope="col">Quantità</th>
                <th id="tipo" scope="col">Tipo</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($table as $articoloStock): ?>
            <tr>
                <th scope="row" id="codiceArticolo<?php echo $articoloStock["Codice"]?>">
                    <?php echo $articoloStock["Codice"]?></th>
                <td headers="nome articolo"><?php echo $articoloStock["Nome"]?></td>
                <td headers="prezzo articolo"><?php echo $articoloStock["PrezzoUnitario"]?></td>
                <td headers="quantità articolo">
                    <input title="UpdateQuantity<?php echo $articoloStock["Codice"]?>" type="number" min="1"
                        class="form-control px-2" id="UpdateQuantity<?php echo $articoloStock["Codice"]?>"
                        value=<?php echo $articoloStock["Quantità"]?>>
                </td>
                <td headers="tipo articolo"><?php echo $articoloStock["Tipo"]?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
            <form id="registerForm" method="post" role=" form">
                <div class="row pt-3 px-5 py-4">
                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="nameInput">Nome</label>
                        <input type="text" class="form-control" id="nameInput" name="name" aria-describedby="nameInput"
                            aria-required="true" required />

                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="surnameInput">Cognome</label>
                        <input type="text" class="form-control" id="surnameInput" name="surname"
                            aria-describedby="surnameInput" aria-required="true" required />
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="birthInput">Data di nascita</label>
                        <input type="date" class="form-control" id="birthInput" name="birthDate"
                            aria-describedby="birthInput" aria-required="true" required />
                    </div>


                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="cityInput">Città</label>
                        <input type="text" class="form-control" id="cityInput" name="city" aria-describedby="cityInput"
                            aria-required="true" aria-required="true" required />
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="addressInput">Via</label>
                        <input type="text" class="form-control" id="addressInput" name="address"
                            aria-describedby="addressInput" aria-required="true" required />
                    </div>

                    <div class="col-12 col-md-1 py-3">
                        <label class="font-weight-bold" for="civicInput">N. civico</label>
                        <input type="number" min="0" class="form-control" id="civicInput" name="civic"
                            aria-describedby="civicInput" aria-required="true" required />
                    </div>
                    <div class="col-12 col-md-5 py-3">
                        <label class="font-weight-bold" for="phone">Telefono (facoltativo)</label>
                        <input type="tel" class="form-control" id="phone" name="phone" pattern="[0-9]{10}"
                            aria-required="false" />
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="societyInput">Azienda (facoltativo)</label>
                        <input type="text" class="form-control" id="societyInput" name="company"
                            aria-describedby="societyInput" aria-required="false" />
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="emailInput">Email</label>
                        <input type="email" class="form-control" id="emailInput" name="email"
                            aria-describedby="emailInput" placeholder="e-mail" required />
                        <div id="errorRegister" class="invalid-feedback hide mb-1">
                            Email errata o già presente
                        </div>
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="password">Password</label>
                        <input type="password" minlength="6" class="form-control" id="password" name="password"
                            aria-required="true" required />
                        <small id="passwordHelpInline" class="text-muted">
                            Lunghezza: minimo 6 caratteri
                        </small>
                    </div>

                    <div class="col-12 col-md-6 py-3">
                        <label class="font-weight-bold" for="confirmPassword">Conferma password</label>
                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"
                            aria-required="true" required />
                    </div>

                    <div class="col-12 px-4 py-1">
                        <input type="checkbox" class="form-check-input px-5" id="personalData" name="personalData"
                            aria-required="true" required />
                        <label class="form-check-label" for="personalData">Acconsento al trattamento dei dati
                            personali</label>
                    </div>

                    <div class="col-12 px-4 py-1">
                        <input type="checkbox" class="form-check-input" id="commercialData" name="commercialData"
                            aria-required="true" required />
                        <label class="form-check-label" for="commercialData">Acconsento all'utilizzo della mail a fini
                            commerciali</label>
                    </div>

                    <div class="col-12 col-md-6 mx-auto px-4 py-2">
                        <button id="registerButton" type="submit"
                            class="btn btn-primary btn-dark btn-block">Registrati</button>
                    </div>
                </div>
            </form>

            <!-- Modal -->
            <div class="modal fade" id="registerModal" role="dialog" aria-hidden="true" tabindex="-1">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                Benvenuto in Borboni Luce!
                            </h4>
                            <button type="button" class="close" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Registrazione effettuata con successo
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="okButton" class="btn btn-primary" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
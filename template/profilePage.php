            <div class="row">
                <div class="col-6 px-0 pt-3">
                    <ul class="border-bottom" style="list-style: none;">
                        <li>
                            <p style="font-size: 1.5em" class="font-weight-bold" id="profileLab">
                                <em class="fas fa-user-circle"></em>
                                Profilo
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-6 text-right pt-3">
                    <a href="modifyProfile.php" class="btn btn-light mx-2" id="updateInt"
                        style="padding:0; font-size: 1.6rem">
                        <em class="fas fa-pen"></em>
                    </a>
                    <button type="button" class="btn btn-light mx-2" id="trashInt" style="padding:0; font-size: 1.6rem"
                        data-toggle="modal" data-target="#deleteModal">
                        <em class="fas fa-trash text-danger"></em>
                    </button>

                </div>
            </div>

            <div class="row">
                <div class="col-12 p-3 text-center">
                    <?php if($profileVal[0]["Profilo"] == null): ?>
                    <img id="imageResult" src="res/defaultProfileImg.png"
                        class="img-fluid rounded-circle mx-auto d-block" />
                    <?php else: ?>
                    <img id="imageResult" src="res/<?php echo $profileVal[0]["Profilo"] ?>"
                        class=" img-fluid rounded-circle mx-auto d-block" />
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <p style="font-size: 2.5em">
                        <?php echo $profileVal[0]["Nome"];
                        echo " " . $profileVal[0]["Cognome"]; ?>
                    </p>
                </div>
            </div>
            <div class="row py-3 ml-1">
                <div class="col-12">
                    <h4 class="font-weight-bold">Dati Personali:</h4>
                </div>
            </div>
            <div class="row ml-2">
                <div class="col-12 col-md-6">
                    <p>Data di nascita: <?php echo $profileVal[0]["DataDiNascita"]; ?></p>
                </div>
                <div class="col-12 col-md-6">
                    <p>Email: <?php echo $profileVal[0]["Email"]; ?></p>
                </div>
                <div class="col-12 col-md-6">
                    <p>Città: <?php echo $profileVal[0]["Città"]; ?> </p>
                </div>
                <div class="col-12 col-md-6">
                    <p>Indirizzo: <?php echo $profileVal[0]["Via"];
echo " " . $profileVal[0]["NumeroCivico"]; ?></p>
                </div>
                <div class="col-12 col-md-6">
                    <p>Telefono: <?php echo $profileVal[0]["Telefono"]; ?></p>
                </div>
                <div class="col-12 col-md-6">
                    <p>Azienda: <?php echo $profileVal[0]["Azienda"]; ?></p>
                </div>
                <?php if($profileVal[0]["Tipo"] == "Admin"): ?>
                <div class="col-12 col-md-6">
                    <p class="text-primary font-weight-bold">Ruolo: <?php echo $profileVal[0]["Tipo"]; ?></p>
                </div>
                <div class="col-12 col-md-6"></div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-4 mx-auto py-3">
                <input type="submit" id="logoutButton" class="btn btn-danger btn-block" value="Logout" />
                <script>
                $(document).ready(function() {
                    $("#logoutButton").click(function() {
                        $.ajax({
                            type: "POST",
                            url: "operationCases.php",
                            data: {
                                action: "Logout"
                            }
                        }).done(function() {
                            $("#logoutModal").modal();
                        });
                    });
                    $("#okButton").click(function() {
                        window.location.href = "index.php";
                    });
                });
                </script>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="logoutModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"> Logout </h4>
                            <button type="button" class="close" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Confermare il logout?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" id="okButton"
                                data-dismiss="modal">Conferma</button>
                            <button type="button" class="btn btn-secondary" id="annullaLogout"
                                data-dismiss="modal">Annulla</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="deleteModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"> Elimina profilo </h4>
                            <button type="button" class="close" data-dismiss="modal"></button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Eliminare il profilo definitivamente?<br />Una volta cancellato non sarà più possibile
                                accedervi.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" id="okDelete"
                                data-dismiss="modal">Conferma</button>
                            <button type="button" class="btn btn-secondary" id="annullaDelete"
                                data-dismiss="modal">Annulla</button>
                        </div>
                    </div>
                </div>
            </div>



            <script>
$("#okDelete").click(function(e) {
    e.preventDefault();
    let email = '<?php echo $profileVal[0]["Email"]; ?>';
    let action = 9;
    $.ajax({
        url: "operationCases.php",
        type: "POST",
        data: {
            action: action,
            email: email
        }
    }).done(function() {
        window.location.href = "login.php";
    });
});
            </script>
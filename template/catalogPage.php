            <div class="row">
                <div class="col-12 px-0 pt-3">
                    <aside>
                        <ul class="border-bottom" style="list-style: none; font-size: 1.5em">
                            <li>
                                <p class="font-weight-bold" id="categoria">
                                    <?php 
                                    if(Title == "<title>Catalogo Interni</title>"){
                                        echo "&#127968; Prodotti per interni";
                                    }elseif (Title == "<title>Catalogo Esterni</title>") {
                                        echo "&#127795; Prodotti per esterni";
                                    }elseif (Title == "<title>Catalogo Pannelli Solari</title>") {
                                        echo "&#127774; Pannelli Solari";
                                    }else {
                                        echo "&#128269; Risultati ricerca";
                                    }
                                ?>
                                </p>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
            <div class="row">
                <?php foreach($templateParams["articoli"] as $articolo):?>
                <section id="article"
                    class="col-12 col-md-5 mx-auto my-3 shadow p-3 bg-white rounded position-relative">
                    <div class="row">
                        <div class="col-12 col-md-7 p-4 text-center">
                            <img src="<?php echo IMG_DIR.$articolo["Immagine"]?>" class="img-fluid"
                                alt="Immagine articolo" />
                        </div>
                        <div class="col-12 col-md-5 p-4" id="articleDesc">
                            <p class="font-weight-bold" style="font-size:1.5em;">
                                <?php echo $articolo["Nome"]?>
                                <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin"): ?>
                                <a title="bottone per modificare l'articolo" style="text-decoration: none;"
                                    class="text-dark"
                                    href="managerUpdateArticle.php?articleClicked=<?php echo $articolo["Codice"] ?>">
                                    <i class="fas fa-pen ml-3"></i>
                                </a>
                                <i title="bottone per eliminare l'articolo" class="fas fa-trash ml-3 text-danger"
                                    style="cursor: pointer;" data-toggle="modal"
                                    data-target="#delete<?php echo $articolo["Codice"]?>"></i>
                                <?php endif; ?>
                            </p>

                            <p class="mb-4" style="word-wrap: break-word;"><?php echo $articolo["Descrizione"]?></p>
                            <p class="text-right font-weight-bold pt-2" style="font-size:1.5em;">
                                <?php echo $articolo["PrezzoUnitario"]?>€</p>
                        </div>
                        <!-- modale-->
                        <div class="modal" id="delete<?php echo $articolo["Codice"] ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Confermare l'eliminazione dell'articolo dal
                                            catalogo?
                                        </h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger"
                                            id="Elimina<?php echo $articolo["Codice"]?>"
                                            data-dismiss="modal">Conferma</button>
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Annulla</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]=="Utente"): ?>
                    <a title="link per visualizzare il prodotto" class="stretched-link"
                        href="article.php?articleClicked=<?php echo $articolo["Codice"] ?>"></a>
                    <?php endif; ?>
                </section>
                <?php endforeach; ?>
            </div>
            <?php if(isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin"):?>
            <div class="row flex-row-reverse my-3 mr-3 fixed-bottom">
                <a href="managerAddArticle.php">
                    <i class="fas fa-plus-circle p-2 rounded-circle bg-white" style="font-size: 3.5em; "></i>
                </a>
            </div>
            <?php endif; ?>
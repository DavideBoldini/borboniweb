<?php

require_once("./bootstrap.php");
define("Title", "<title>Mappa del sito</title>");

$css = array("css/index.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "mapPage.php";
$templateParams["head"] = "headPage.php";

require("template/base.php");
?>
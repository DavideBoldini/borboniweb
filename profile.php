<?php

require_once "./bootstrap.php";
define("Title", "<title>Profilo</title>");

$css = array("css/profile.css", "css/header.css");
if (!isset($_SESSION["email"])) {
    header("location: login.php");
} else {
    $profileVal = $dbh->getPersonalInfo($_SESSION["email"]);
}


$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "profilePage.php";
$templateParams["head"] = "headPage.php";

require "template/base.php";

?>
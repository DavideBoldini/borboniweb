<?php

require_once("./bootstrap.php");
define("Title", "<title>Modifica Articolo</title>");

$css = array("css/index.css", "css/header.css");
if (!isset($_SESSION["email"])) {
    header("location: login.php");
}
if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin") {
    
    header("location: index.php");
}

$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "managerUpdateArticlePage.php";
$templateParams["head"] = "headPage.php";
$articleToUpdate = $_GET["articleClicked"];
$templateParams["articleToUpdate"]=$dbh->getArticleByID($articleToUpdate);

require("template/base.php");
?>
<?php

require_once("./bootstrap.php");
define("Title", "<title>Articolo</title>");
$css = array("css/index.css", "css/header.css", "css/article.css");
if($_GET["articleClicked"]==null || (isset($_SESSION["tipo"]) && $_SESSION["tipo"]=="Admin")){
    header("location: index.php");
}

$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "articlePage.php";
$templateParams["head"] = "headPage.php";
$articleClicked = $_GET["articleClicked"];
$templateParams["articleClicked"]=$dbh->getArticleByID($articleClicked);

require("template/base.php");
?>
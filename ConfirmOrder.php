<?php


require_once("./bootstrap.php");
define("Title", "<title>Conferma Ordine</title>");

$css = array("css/index.css", "css/header.css");
if (!isset($_SESSION["email"])) {
    header("location: login.php");
} else {
    $profileVal = $dbh->getPersonalInfo($_SESSION["email"]);
}

$templateParams["totalAmount"] = $dbh->getTotalAmount($_SESSION["email"])[0]["TOTALE"];
$templateParams["articlesNumber"] = $dbh->getNumArticle($_SESSION["email"])[0]["NUMERO_ARTICOLI"];
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "ConfirmOrderFormPage.php";
$templateParams["head"] = "headWithJSPage.php";

require("template/base.php");
?>
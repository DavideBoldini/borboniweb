<?php

require_once("./bootstrap.php");
define("Title", "<title>Vendite</title>");

if(!isset($_SESSION["tipo"]) || $_SESSION["tipo"]!="Admin") {
    
    header("location: index.php");
}
$css = array("css/info.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "salesPage.php";
$templateParams["head"] = "headPage.php";
$table = $dbh->getVendite();

if(count($_GET)!=0 && $_GET["salesArticleCode"]!=""){
    $articleSales = $dbh->searchArticleInSales($_GET["salesArticleCode"]);
    $table = $articleSales;
}

require("template/base.php");
?>
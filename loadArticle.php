<?php 
include 'loadImage.php';
require_once("./bootstrap.php");

list($result,$msg) = uploadImage(IMG_DIR,$_FILES["uploadImg"]);
if($result==1){         //caricata correttamente
    $imgProduct = $msg;
    $dbh->addNewArticle($_POST["addName"], $_POST["addDesc"], $_POST["addPrice"], $_POST["addType"], $imgProduct);
    $code = $dbh->getLatestCode()[0]["MAXCODE"];
    $dbh->insertBroadCastNotificationAdmins("NUOVO ARTICOLO", "Aggiunto nuovo articolo ".$_POST["addName"]." con codice: ".$code);    
    $dbh->addArticleToStock($code); 
    $dbh->addArticleVendite($code);        
}  

?>
<?php

require_once("./bootstrap.php");
define("Title", "<title>FAQ</title>");

$css = array("css/index.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "faqPage.php";
$templateParams["head"] = "headPage.php";

require("template/base.php");
?>
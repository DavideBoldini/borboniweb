<?php

require_once("./bootstrap.php");
define("Title", "<title>Profilo</title>");

$css = array("css/profile.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "modifyProfilePage.php";
$templateParams["head"] = "headWithJSPage.php";

$profileVal = $dbh->getPersonalInfo($_SESSION["email"]);

require "template/base.php";

?>
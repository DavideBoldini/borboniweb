<?php

class DatabaseHelper{

        private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if($this->db->connect_error){
            die("Connessione al db fallita");
        }

    }
    /*METODI PER LE FUNZIONALITA' DESIDERATE CHE SI INTERFACCIANO COL DATABASE*/

    public function getArticleByID($id){
        $stmt = $this->db->prepare("SELECT * FROM articolo WHERE Codice = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getLatestArticle($n=3){
        $stmt = $this->db->prepare("SELECT Immagine, Codice FROM articolo ORDER BY DATE(Data)desc LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getChristmasArticle($n=3){
        $stmt = $this->db->prepare("SELECT Immagine, Codice FROM articolo WHERE MONTH(Data) = 12 LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }

    public function getFeaturedArticle($n=3){
        $stmt = $this->db->prepare("SELECT Immagine, articolo.Codice FROM articolo, vendite WHERE articolo.Codice = vendite.Codice ORDER BY Quantità DESC LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPersonalInfo($email){
        $stmt = $this->db->prepare("SELECT * FROM utente WHERE Email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertNewUser($email, $password, $nome, $cognome, $dataNascita, $città, $tipo, $telefono, $via, $numCivico, $azienda){
        $stmt = $this->db->prepare("INSERT INTO utente VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL)");
        $stmt->bind_param("sssssssssis", $email, $password, $nome, $cognome, $dataNascita, $città, $tipo, $telefono, $via, $numCivico, $azienda);
        return $stmt->execute();
    }

    public function getInteriorCatalogue(){
        $stmt = $this->db->prepare("SELECT * FROM articolo WHERE Tipo = 'Interni'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getExteriorCatalogue(){
        $stmt = $this->db->prepare("SELECT * FROM articolo WHERE Tipo = 'Esterni'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSolarPanelsCatalogue(){
        $stmt = $this->db->prepare("SELECT * FROM articolo WHERE Tipo = 'Pannelli Solari'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password){
        $stmt = $this->db->prepare("SELECT * FROM utente WHERE Email = ? AND BINARY Password = ?");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function insertArticleToShoppingCart($email, $codice){
        $stmt = $this->db->prepare("INSERT INTO carrello VALUES (?, ?, 1)");
        $stmt->bind_param("si", $email, $codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function getShoppingCart($email){
        $stmt = $this->db->prepare("SELECT Nome, articolo.Codice, PrezzoUnitario, Immagine, Quantità, Tipo FROM carrello, articolo WHERE carrello.Codice=articolo.Codice AND Email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTotalAmount($email){
        $stmt = $this->db->prepare("SELECT SUM(PrezzoUnitario * Quantità) AS TOTALE FROM carrello, articolo WHERE carrello.Codice=articolo.Codice AND Email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNumArticle($email){
        $stmt = $this->db->prepare("SELECT COUNT(Email) AS NUMERO_ARTICOLI FROM carrello WHERE Email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateQuantityArticle($codice, $quantità, $email){
        $stmt = $this->db->prepare("UPDATE carrello SET Quantità=? WHERE carrello.Codice=? AND Email=?");
        $stmt->bind_param("iis", $quantità, $codice, $email);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function deleteArticleFromCart($codice, $email){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE Codice=? AND Email=?");
        $stmt->bind_param("is", $codice, $email);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function deleteAllArticleFromCart($email){
        $stmt = $this->db->prepare("DELETE FROM carrello WHERE Email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function updateArticle($codice, $nome, $descrizione, $prezzo){
        $stmt = $this->db->prepare("UPDATE articolo SET Nome=?, Descrizione=?, PrezzoUnitario=? WHERE Codice=?");
        $stmt->bind_param("ssdi", $nome, $descrizione, $prezzo,  $codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function addNewArticle($nome, $descrizione, $prezzo, $tipo, $immagine){
        $stmt = $this->db->prepare("INSERT INTO articolo VALUES (DEFAULT, ?, ?, ?, ?,CURDATE(), ?)");
        $stmt->bind_param("ssdss", $nome, $descrizione, $prezzo, $tipo, $immagine);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function deleteArticleFromCatalogue($codice){
        $stmt = $this->db->prepare("DELETE FROM articolo WHERE Codice=?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function addArticleToStock($codice){
        $stmt = $this->db->prepare("INSERT INTO giacenza VALUES (?, 1)");
        $stmt->bind_param("i",$codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function updateStock($codice, $quantità){
        $stmt = $this->db->prepare("UPDATE giacenza SET Quantità=? WHERE Codice=?");
        $stmt->bind_param("ii", $quantità, $codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function getStock(){
        $stmt = $this->db->prepare("SELECT * FROM giacenza, articolo WHERE articolo.Codice=giacenza.Codice");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getQuantityArticleInStock($codice){
        $stmt = $this->db->prepare("SELECT Quantità FROM giacenza WHERE Codice=?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function searchArticle($nome){
        $stmt = $this->db->prepare("SELECT * FROM articolo WHERE Nome LIKE '%".$nome."%' ");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function searchArticleInStock($codice){
        $stmt = $this->db->prepare("SELECT * FROM giacenza, articolo WHERE articolo.Codice=giacenza.Codice AND giacenza.Codice = ?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateUserInfo($email, $nome, $cognome, $dataNascita, $città, $via, $numCivico, $telefono, $azienda, $immagine){
        $stmt = $this->db->prepare("UPDATE utente SET Nome=?, Cognome=?, DataDiNascita=?, Città=?, Via=?, NumeroCivico=?, Telefono=?, Azienda=?, Profilo = ? WHERE Email=?");
        $stmt->bind_param("sssssissss", $nome, $cognome, $dataNascita, $città, $via, $numCivico, $telefono, $azienda, $immagine, $email);
        return $stmt->execute();
        
    }

    public function deleteUser($email){
        $stmt = $this->db->prepare("DELETE FROM utente WHERE Email=?");
        $stmt->bind_param("s", $email);
        return $stmt->execute();
        
    }

    public function insertNotification($email, $titolo, $descrizione){
        $stmt = $this->db->prepare("INSERT INTO notifica VALUES(DEFAULT, ?, CURDATE(), CURTIME(), ?, ?)");
        $stmt->bind_param("sss", $email, $titolo, $descrizione);
        return $stmt->execute();
    }

    public function getNotifications($email){
        $stmt = $this->db->prepare('SELECT Codice, Email, Data, TIME_FORMAT(Orario, "%H:%i") as Orario, Titolo, Descrizione FROM notifica WHERE Email = ? ORDER BY "Data" DESC, Orario DESC');
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAdmins(){
        $stmt = $this->db->prepare("SELECT Email FROM utente WHERE Tipo = 'Admin'");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertBroadCastNotificationAdmins($titolo, $descrizione){
        $AdminEmails = $this->getAdmins();    
        foreach($AdminEmails as $adminEmail){
            $email = $adminEmail["Email"];
            $stmt = $this->db->prepare("INSERT INTO notifica VALUES(DEFAULT, ?, CURDATE(), CURTIME(), ?, ?)");
            $stmt->bind_param("sss", $email, $titolo, $descrizione);
            $stmt->execute();
        }
    }

    public function deleteNotification($codice){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE Codice=?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
    }

    public function getLatestCode(){
        $stmt = $this->db->prepare("SELECT MAX(Codice) as MAXCODE FROM articolo");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteAllNotifications($email){
        $stmt = $this->db->prepare("DELETE FROM notifica WHERE Email=?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
    }

    public function updateVendite($codice, $quantità){
        $stmt = $this->db->prepare("UPDATE vendite SET Quantità=? WHERE Codice=?");
        $stmt->bind_param("ii", $quantità, $codice);
        $stmt->execute();
    }

    public function getVendite(){
        $stmt = $this->db->prepare("SELECT * FROM articolo, vendite WHERE articolo.Codice = vendite.codice");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getQuantitySales($codice){
        $stmt = $this->db->prepare("SELECT Quantità FROM vendite WHERE Codice=?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addArticleVendite($codice){
        $stmt = $this->db->prepare("INSERT INTO vendite VALUES (?, 0)");
        $stmt->bind_param("i",$codice);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    public function searchArticleInSales($codice){
        $stmt = $this->db->prepare("SELECT * FROM vendite, articolo WHERE articolo.Codice=vendite.Codice AND vendite.Codice = ?");
        $stmt->bind_param("i", $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function searchTopnArticleInSales($n=10){
        $stmt = $this->db->prepare("SELECT * FROM vendite, articolo WHERE articolo.Codice=vendite.Codice ORDER BY Quantità DESC LIMIT ?");
        $stmt->bind_param("i", $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}

?>
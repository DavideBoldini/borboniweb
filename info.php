<?php

require_once("./bootstrap.php");
define("Title", "<title>Info</title>");

$css = array("css/info.css", "css/header.css");
$templateParams["header"] = "headerPage.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "infoPage.php";
$templateParams["head"] = "headPage.php";


require("template/base.php");
?>
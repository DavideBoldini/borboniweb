<?php

/*  1 add article
2 delete article
3 delete all article
4 aggiorna quantità articolo nel carrello
5 aggiorna articolo (lato Admin)
6 cancella articolo da catalogo
7 aggiorna quantità
8 aggiorna profilo
9 cancella profilo
10 cancella notifica
11 cancella tutte le notifiche
 */
require_once("./bootstrap.php");

switch ($_POST["action"]) {
    case 1:
        if($dbh->getQuantityArticleInStock( $_POST["code"])[0]["Quantità"] > 0){
            $dbh->insertArticleToShoppingCart($_SESSION["email"], $_POST["code"]);
        }
        break;
    case 2:$dbh->deleteArticleFromCart($_POST["code"], $_SESSION["email"]);
        break;
    case 3:
         foreach($_SESSION["articlesInCart"] as $articolocarrello){
                $stockQuantity = $dbh->getQuantityArticleInStock($articolocarrello["Codice"])[0]["Quantità"];
                $dbh->updateStock($articolocarrello["Codice"], $stockQuantity - $articolocarrello["Quantità"]);
                $salesQuantity = $dbh->getQuantitySales($articolocarrello["Codice"])[0]["Quantità"];
                $dbh->updateVendite($articolocarrello["Codice"], $salesQuantity + $articolocarrello["Quantità"]);
            }
           $dbh->deleteAllArticleFromCart($_SESSION["email"]);
           $date = date_add(new DateTime(), date_interval_create_from_date_string("7 days"));
           $date = $date->format('d-m-Y');
           $dbh->insertNotification($_SESSION["email"], "CONSEGNA PREVISTA", "La consegna con riferimento all'ordine appena effettuato è prevista entro il giorno ".$date);
        break;
    case 4: 
        $stockQuantity = $dbh->getQuantityArticleInStock($_POST["code"])[0]["Quantità"];
        if( $stockQuantity >= $_POST["quantity"]){ 
            if( $stockQuantity == $_POST["quantity"]){
                $dbh->insertBroadCastNotificationAdmins("SCORTE TERMINATE", "Le scorte del prodotto ".$_POST["code"]." sono esaurite. Si invita l'utente a controllare la giacenza");
            }
            $dbh->updateQuantityArticle($_POST["code"], $_POST["quantity"], $_SESSION["email"]);
           
        }
        else{
            $dbh->insertBroadCastNotificationAdmins("ESAURIMENTO SCORTE", "Tentativo ordine per il prodotto ".$_POST["code"]." superiore alla giacenza. Si invita l'utente a controllare le scorte");
        }    
        break;
    case 5:$dbh->updateArticle($_POST["code"], $_POST["newName"], $_POST["newDesc"], $_POST["newPrice"]);
        break;
    case 6:$dbh->deleteArticleFromCatalogue($_POST["code"]);
        break;
    case 7:$dbh->updateStock($_POST["code"], $_POST["quantity"]);
        break;
    case 8: //$dbh->updateUserInfo($_POST["email"], $_POST["name"], $_POST["surname"], $_POST["birthDate"], $_POST["city"], $_POST["address"], $_POST["civic"], $_POST["phoneNumber"], $_POST["societyInput"], $_POST["profileInput"]);
        //break;
    case 9: 
        unset($_SESSION["email"]);
        unset($_SESSION["tipo"]);
        $dbh->deleteUser($_POST["email"]);
        break;
    case "Logout":
        unset($_SESSION["email"]);
        unset($_SESSION["tipo"]);
        break;
    case 10:$dbh->deleteNotification($_POST["code"]);
        echo count($dbh->getNotifications($_SESSION["email"]));
        ?>
<script>
$("#notificationList").load(location.href + " #notificationList");
</script>
<?php
    break;
    case 11:  $dbh->deleteAllNotifications($_SESSION["email"]);
          echo count($dbh->getNotifications($_SESSION["email"]));
    break;
    case 12:
        if (isset($_POST["form"][0]["value"]) && isset($_POST["form"][1]["value"])) {
            $loginVal = $dbh->checkLogin($_POST["form"][0]["value"], $_POST["form"][1]["value"]);
            if (count($loginVal) != 0) {
                $_SESSION["email"] = $loginVal[0]["Email"];
                $_SESSION["tipo"] = $loginVal[0]["Tipo"];
                echo "true";
                break;
            }
        }
        echo "false";
        break;
    case 13:
        if(isset($_SESSION["email"])){
            unset($_SESSION["email"]);
        }
        if(isset($_SESSION["tipo"])){
            unset($_SESSION["tipo"]);
        }
        $name = $_POST["form"][0]["value"];
        $surname = $_POST["form"][1]["value"];
        $birthDate = $_POST["form"][2]["value"];
        $city = $_POST["form"][3]["value"];
        $address = $_POST["form"][4]["value"];
        $civic = $_POST["form"][5]["value"];
        $phone = $_POST["form"][6]["value"];
        $company = $_POST["form"][7]["value"];
        $email = $_POST["form"][8]["value"];
        $password = $_POST["form"][9]["value"];
        
        if ($phone == "") {
            $phone = null;
        }
        if ($company == "") {
            $company = null;
        }
        $registerVal = $dbh->insertNewUser($email, $password, $name, $surname, $birthDate, $city,
            "Utente", $phone, $address, $civic, $company);
        if ($registerVal) {
            //Se registrazione corretta
            echo "true";
            break;
        }
        echo "false";
        break;
            
}

?>
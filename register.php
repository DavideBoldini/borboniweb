<?php

require_once "./bootstrap.php";

define("Title", "<title>Registrati</title>");
$css = array("css/register.css");
$templateParams["headerLR"] = "headerLR.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "registerPage.php";
$templateParams["head"] = "headWithJSPage.php";

require "template/base.php";

?>
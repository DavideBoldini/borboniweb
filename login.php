<?php

require_once("./bootstrap.php");

define("Title", "<title>Accedi</title>");

$css = array("css/login.css");
$templateParams["headerLR"] = "headerLR.php";
$templateParams["footer"] = "footerPage.php";
$templateParams["body"] = "loginPage.php";
$templateParams["head"] = "headWithJSPage.php";
require("template/base.php");
?>
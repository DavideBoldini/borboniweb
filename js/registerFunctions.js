$(document).ready(function() {

    $("#okButton").click(function() {
        window.location.href = "login.php";
    });

    
    $("#registerForm").submit(function(e) {
        e.preventDefault();
        let password = $("#password").val();
        let confirmPassword = $("#confirmPassword").val();

        if (password != confirmPassword || password.length < 6) {
            document.getElementById("password").className += " is-invalid";
            document.getElementById("confirmPassword").className +=
                " is-invalid";
            return false;
        }
        let form = $("#registerForm").serializeArray();
        let action = 13;
        $.ajax({
            type: 'POST',
            url: 'operationCases.php',
            data: {
                action: action,
                form: form
            },
            success: function(e) {
                if (e == "true") {
                    $("#registerModal").modal({backdrop: "static", keyboard: false});
                    $("#registerModal").modal("show");
                } else {
                    document.getElementById("emailInput")
                        .className += " is-invalid";
                    $("#errorRegister").show();
                }
            }
        });
    });
});
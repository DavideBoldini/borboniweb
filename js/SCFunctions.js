$(document).ready(function() {
    $('button[id^="Rimuovi"]').click(function() {
        let action = 2 //delete single article in the cart
        let code = $(this).attr('id').substring(7);
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                action: action
            }
        }).done(function() {
            location.reload();
        });
    });

    $('Input[id^="Quantity"]').change(function() {
        let action = 4 //update quantity article in the cart
        let code = $(this).attr('id').substring(8);
        let quantity = document.getElementById($(this).attr('id')).value;

        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                quantity: quantity,
                action: action
            }
        }).done(function() {
            $("#checkOrder").load(window.location.href + " #checkOrder");
            $("#QuantityStock"+code).load(window.location.href + " #QuantityStock"+code);
            
        });

    });

    $('Input[id^="Quantity"]').focusout(function() {
        location.reload();
    });
});
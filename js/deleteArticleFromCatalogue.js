$(document).ready(function() {
    $('button[id^="Elimina"]').click(function() {
        let action = 6 //delete single article in the cart
        let code = $(this).attr('id').substring(7);
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                action: action
            }
        }).done(function() {
            location.reload()
        });
    });
});
$(document).ready(function() {
 
    if(document.referrer === "http://localhost/BorboniWEB/borboniweb/catalogInt.php"){
        $("#catalogInterni").attr("selected", true);
    }
    else if(document.referrer === "http://localhost/BorboniWEB/borboniweb/catalogExt.php"){
        $("#catalogoEsterni").attr("selected", true); 
    }
    else{
        $("#catalogoPanSolari").attr("selected", true);
    }

    $("#formArticle").submit(function(e) {
        e.preventDefault();
        let dati = new FormData(this);
        $.ajax({
            type: "POST",
            data: dati,
            url: "loadArticle.php",
            cache: false,
            contentType: false,
            processData: false,
            success: function(e) {
                $("#articleModal").modal({backdrop: "static", keyboard: false});
                $("#articleModal").modal("show");
            }
        });
    });
});
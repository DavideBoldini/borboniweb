$(document).ready(function(){

    $("#uploadImg").change(function(){
        const imageToUpload = $("#uploadImg")[0];
        if (imageToUpload.files && imageToUpload.files[0]) {
            const reader = new FileReader();
    
            reader.onload = function (e) {
                document.getElementById("imageResult")
                    .setAttribute('src', e.target.result);
            };
            reader.readAsDataURL(imageToUpload.files[0]);
        }
    });
});
$(document).ready(function(){

    $("#okButton").click(function() {
        window.location.href = "index.php";
    });

    
    $("#loginForm").submit(function(e) {
        e.preventDefault();
        let form = $("#loginForm").serializeArray();
        let action = 12;
        $.ajax({
            type: 'POST',
            url: 'operationCases.php',
            data: {
                action: action,
                form: form
            },
            success: function(e) {
                if (e == "true") {
                    $("#loginModal").modal({backdrop: "static", keyboard: false});
                    $("#loginModal").modal("show");
                } else {
                    document.getElementById("InputPassword").className += " is-invalid";
                    document.getElementById("InputEmail").className += " is-invalid";
                    $("#errorLogin").show();
                }
            }
        });
    });

});

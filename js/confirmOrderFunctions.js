$(document).ready(function() {
    $("#undo").click(function() {
        window.location.href = "shoppingCart.php";
    });

    $("#ConfirmOrderButton").click(function() {   

        let requiredFields = ["cityInput", "addressInput", "civicInput"];
        let i = 0;
        for (i = 0; i < requiredFields.length; i++) {
            if (document.getElementById(requiredFields[i]).value == "") {
                document.getElementById(requiredFields[i]).className += " is-invalid";
                return false;
            }
        }

        const fixedNArt = document.createElement("p");
        fixedNArt.innerHTML+=document.getElementById("numArticles").placeholder;
        document.getElementById("numArticles").replaceWith(fixedNArt);

        const fixedCity = document.createElement("p");
        fixedCity.innerHTML+=document.getElementById("cityInput").value;
        document.getElementById("cityInput").replaceWith(fixedCity);

        const fixedAddress = document.createElement("p");
        fixedAddress.innerHTML+=document.getElementById("addressInput").value;
        document.getElementById("addressInput").replaceWith(fixedAddress);

        const fixedCivic = document.createElement("p");
        fixedCivic.innerHTML+=document.getElementById("civicInput").value;
        document.getElementById("civicInput").replaceWith(fixedCivic);

        let current_datetime = new Date()
        let formatted_date = current_datetime.getDate() + "_" + (current_datetime.getMonth() + 1) + "_" + current_datetime.getFullYear()

        //element contiene tutto ciò che verrà stampato nel riepilogo ordine
        const element = document.getElementById("toPrint");
        html2pdf()
            .from(element)
            .save('RiepilogoOrdine_'+ formatted_date);        

        
        let action = 3 //delete all article in the cart
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                action: action
            }
        }).done(function() {
            window.scroll({
                top: 0,
                behavior: "smooth"
            })
            $("#ConfirmOrderButton").hide();
            $("#undo").hide();
        });
    });

    $("#backHome").click(function() {
        window.location.href = "index.php";
    });

});
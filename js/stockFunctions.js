$(document).ready(function() {
    $('Input[id^="UpdateQuantity"]').focusin(function() {
        let action = 7 //update quantity article in the cart
        let code = $(this).attr('id').substring(14);
        let quantity = document.getElementById($(this).attr('id')).value;
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                quantity: quantity,
                action: action
            }
        })
    });

    $('Input[id^="UpdateQuantity"]').focusout(function() {
        let action = 7 //update quantity article in the cart
        let code = $(this).attr('id').substring(14);
        let quantity = document.getElementById($(this).attr('id')).value;
        $.ajax({
            url: "operationCases.php",
            type: "POST",
            data: {
                code: code,
                quantity: quantity,
                action: action
            }
        }).done(function() {
            location.reload()
        });
    });
})